
package calculadora;

import java.util.Scanner;

/**Programa para calcular suma, resta, producto y cociente.
 * @since 7.3.1
 * @version 7.2, 2013/10/10
 * @author ycasalquintas
 */
public class Calculadora {
    static float num1;
    static float num2;
    static float resultado;
    static int operacion;

    public static void main(String[] args) {
      System.out.println("Elija la operación que quiere realizar\n1-Suma\n2-Resta\n3-Producto\n4-Cociente");
        Scanner obx = new Scanner(System.in);
         operacion = obx.nextInt();
      System.out.println("Introduce el primer número:");
        num1 = obx.nextFloat();
       System.out.println("Introduce el segundo número:");
        num2 = obx.nextFloat();
        if (operacion == 1) {
            resultado = suma(num1, num2);
        } else if (operacion == 2) {
            resultado = resta(num1, num2);
        } else if (operacion == 3) {
            resultado = producto(num1, num2);
        } else if (operacion == 4) {
            resultado = cociente(num1, num2);
    }
   System.out.println("Resultado= " + resultado);
    }
    
    public static float suma(float no1, float n2) {
        return no1 + num2;
        //@return suma de ambos números
    }

    public static float resta(float no1, float n2) {
        return no1 - num2;
        //@return Resta de ambos números
    }
 
    public static float producto(float no1, float n2) {
        return no1 * num2;
      // @return multiplicación de ambos números 
    }

      public static float cociente(float no1, float n2) {
        return no1 / num2;
        //@return El cociente de los dos
    }
}